package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.ArrayList;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.contains(null)) 
            throw new CannotBuildPyramidException();

        double h = 0;
        double w = 0;
        
        // height is getting from sum of arithmetic progression...
        h = (-1 + Math.sqrt(1 + 8*inputNumbers.size()))/2;
        
        if (h > Integer.MAX_VALUE || h != (int) h) 
            throw new CannotBuildPyramidException();
        
        w = 1 + 2*(h-1);
        
        if (w > Integer.MAX_VALUE) 
            throw new CannotBuildPyramidException();
        
        int hi = (int) h, 
            wi = (int) w;
        
        int m[][] = new int[hi][wi];
        
        // to avoid mutation of input List
        // and in case if we have an immutable List
        // we are creating the new one to sort it
        List<Integer> al = new ArrayList<>(inputNumbers);
        
        // in case if we will have the very big positive
        // and the very big negative integers in the list,
        // lambda (a,b) -> a-b may lead to overflow results
        // that will compromise our sort, so we better
        // to use full comparisons statement
        al.sort( (a,b) -> a < b ? -1 : a > b ? 1 : 0 );
        
        int l = 0;
        // k - is the position of the first elements in the respective rows
        // that will be containing the values from the List.
        // It starts from center and shifts left by one on every next row.
        for (int i = 0, k = wi/2; i < hi; i++, k--) {
            for (int j = k; j+k < wi; j += 2) {
                m[i][j] = al.get(l++);
            }
        }
        
        return m;
    }
}
