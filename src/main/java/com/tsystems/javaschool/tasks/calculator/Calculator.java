package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Deque;
import java.util.List;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.math.RoundingMode;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // In case if future logic of method will be changed
        // the check of statement length is also may be necessary.
        if (statement == null)
            return null;
        
        Buffer buf = new Buffer();
        
        int i = 0, j,
            len = statement.length();
        
        char c;
        
        while (i < len) {
            c = statement.charAt(i);

            if (c == '(') {
                buf.pushLevel();
                i++;

            } else if (c == ')') {
                try {
                    buf.addNumber(calcToNumber(buf.popLevel()));
                } catch (CalcToNumberException|ArithmeticException e) {
                    return null;
                }
                i++;
                
            } else if ((c >= '0' && c <= '9') || c == '.') {
                j = i;
                while (++j < len && (((c = statement.charAt(j)) >= '0' 
                        && c <= '9') || c == '.'));
                
                try {
                    buf.addNumber(Double.parseDouble(statement.substring(i, j)));
                } catch (RuntimeException e) {
                    return null;
                }
                i = j;
                
            } else if (c == '*' || c == '+' || c == '-' || c == '/') {
                buf.addOperator(c);
                i++;
                
            } else {
                return null;
            }
        }
        
        // We must to check that all opening parentheseses are paired
        // with the closing ones before proceed to final calculations. 
        // If it is not true, the overall result is null.
        if (buf.haveSingleLevel())
            try {
                return round(calcToNumber(buf));
            } catch (CalcToNumberException|ArithmeticException e) {
                return null;
            }
        else
            return null;
    }
    
    private class Buffer {
        private final Deque<List<Double>> numbersLevels = new ArrayDeque<>();
        private final Deque<List<Character>> operatorsLevels = new ArrayDeque<>();

        Buffer() {
            pushLevel();
        }
        
        private Buffer(List<Double> numbers, List<Character> operators) {
            pushLevel(numbers, operators);
        }
        
        void pushLevel() {
            pushLevel(new LinkedList<Double>(), new LinkedList<Character>());
        }
        
        private void pushLevel(List<Double> numbers, List<Character> operators) {
            numbersLevels.push(numbers);
            operatorsLevels.push(operators);
        }
        
        Buffer popLevel() {
            return new Buffer(numbersLevels.pop(), operatorsLevels.pop());
        }
        
        List<Double> peekNumbers() {
            return numbersLevels.peek();
        }
        
        List<Character> peekOperators() {
            return operatorsLevels.peek();
        }
        
        void addNumber(double number) {
            peekNumbers().add(number);
        }
        
        void addOperator(char operator) {
            peekOperators().add(operator);
        }
        
        boolean haveSingleLevel() {
            if (numbersLevels.size() == 1) return true;
            else return false;
        }
    }
    
    private Double calcToNumber(Buffer buf) throws IllegalListSizeRatioException, IllegalOperatorException 
    {
        List<Double> numbers = buf.peekNumbers();
        List<Character> operators = buf.peekOperators();
        
        // Our operators are binary and that fact lead us to current rellation.
        // This check is unnecessary to current implementation of evaluate method,
        // but we'll keep it here to not get this method depended
        // on outer exceptions handling and to not break overall method logic.        
        if (numbers.size() - operators.size() != 1)
            throw new IllegalListSizeRatioException();
            
        for (int i = 0; i < operators.size(); i++) {
            if (operators.get(i) == '/') {
                if (numbers.get(i+1) == 0) 
                    throw new ArithmeticException();
                
                numbers.set(i, numbers.get(i) / numbers.get(i+1));

                numbers.remove(i+1);
                operators.remove(i--);
            } else if (operators.get(i) == '*') {
                numbers.set(i, numbers.get(i) * numbers.get(i+1));

                numbers.remove(i+1);
                operators.remove(i--);
            }
        }

        while (operators.size() > 0) {
            if (operators.get(0) == '+') {
                numbers.set(0, numbers.get(0) + numbers.get(1));   
            } else if (operators.get(0) == '-') {
                numbers.set(0, numbers.get(0) - numbers.get(1));
            } else {
                throw new IllegalOperatorException();
            }
            
            numbers.remove(1);
            operators.remove(0);
        }

        return numbers.get(0);
    }
    
    public class CalculatorException extends Exception { }
    
    private class CalcToNumberException extends CalculatorException { }
    
    private class IllegalListSizeRatioException extends CalcToNumberException { }
    
    private class IllegalOperatorException extends CalcToNumberException { }
    
    private String round(Double number) {
        // This check is unnecessary to current implementation of evaluate method,
        // but we'll keep it here to not get this method depended
        // on outer exceptions handling.
        if (number == null) throw new NullPointerException();
        
        DecimalFormat df = new DecimalFormat("#.####");
        
        // this is need for '.' as decimal point instead locale default ','
        df.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ENGLISH));
        df.setRoundingMode(RoundingMode.CEILING);
        
        return df.format(number);
    }
}